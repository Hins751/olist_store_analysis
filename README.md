**olist_store_analysis**

Olist Store is the largest department store in Brazilian marketplaces. Olist connects small businesses from all over Brazil to channels without hassle and with a single contract. The Brazilian ecommerce public dataset of orders (from 2016 to 2018) made at Olist Store is provided to your company for analysis.

What Should We Do?

1. How many customers, orders, and orders per customer does the company have?
2. What is the number of customers by state?
3. What is the number of orders by month?
4. What are the top 5 product categories?
5. Sales trend
